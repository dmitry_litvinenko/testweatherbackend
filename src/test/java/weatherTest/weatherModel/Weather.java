package weatherTest.weatherModel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Weather {

    private String description;
    private String main;
    private String icon;
    private Integer id;


    public String getDescription() {
        return description;
    }

    public String getMain() {
        return main;
    }

    public String getIcon() {
        return icon;
    }

    public Integer getId() {
        return id;
    }
}

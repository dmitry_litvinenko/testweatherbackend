package weatherTest.weatherModel;

import java.math.BigDecimal;

public class Coordinates {
    private BigDecimal lon;
    private BigDecimal lat;

    public BigDecimal getLon() {
        return lon;
    }

    public BigDecimal getLat() {
        return lat;
    }

}

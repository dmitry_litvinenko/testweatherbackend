package weatherTest.weatherModel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherModel {

    private Coordinates coord;
    private Clouds clouds;
    private List<Weather> weather;

    public List<Weather> getWeather() {
        return weather;
    }

    public Coordinates getCoord() {
        return coord;
    }

    public Clouds getClouds() {
        return clouds;
    }

}

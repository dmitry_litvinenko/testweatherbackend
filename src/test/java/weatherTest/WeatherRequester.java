package weatherTest;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import weatherTest.weatherModel.WeatherModel;

import java.io.IOException;

public class WeatherRequester {

    private String weatherRequest = "http://samples.openweathermap.org/data/2.5/weather?q=Town,uk&appid=b1b15e88fa797225412429c1c50c122a1";

    public WeatherModel getWeather(String cityName) throws IOException {

        String townWeather = weatherRequest.replaceAll("Town", cityName);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity(townWeather, String.class);
        String jsonWeatherResponse = response.getBody();
        ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

        return objectMapper.readValue(jsonWeatherResponse, WeatherModel.class);
    }

}

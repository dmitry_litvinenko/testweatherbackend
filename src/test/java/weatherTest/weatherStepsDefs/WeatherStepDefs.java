package weatherTest.weatherStepsDefs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import weatherTest.WeatherRequester;
import weatherTest.weatherModel.WeatherModel;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import static org.testng.Assert.assertEquals;


public class WeatherStepDefs {

    private String cityName;
    private WeatherModel weatherModel;
    private WeatherRequester weatherRequester = new WeatherRequester();


    @Given("City name is (.*)")
    public void setCityName(String name) {
        cityName = name;
    }

    @When("Requesting weather information")
    public void requestWeatherInformation() throws IOException {
        weatherModel = weatherRequester.getWeather(cityName);
    }

    @Then("Coordinates are lon: (.*) and lat: (.*)")
    public void checkCoordinates(BigDecimal lon, BigDecimal lat) {
        assertEquals(lon, weatherModel.getCoord().getLon());
        assertEquals(lat, weatherModel.getCoord().getLat());
    }

    @Then("Cloud covering are (.*)")
    public void cloudCovering(Integer cloudCovering) {
        assertEquals(cloudCovering, weatherModel.getClouds().getAll());
    }

    @And("Weather description is: (.*) and detailed description is: (.*)")
    public void weatherDescription(String description, String detailedDescription) {
        assertEquals(description, weatherModel.getWeather().get(0).getMain());
        assertEquals(detailedDescription, weatherModel.getWeather().get(0).getDescription());
    }

    @And("Weather id is: (.*) and weather icon is: (.*)")
    public void weather(Integer id, String icon) {
        assertEquals(id, weatherModel.getWeather().get(0).getId());
        assertEquals(icon, weatherModel.getWeather().get(0).getIcon());
    }
}



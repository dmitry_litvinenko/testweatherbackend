Feature: Weather Feature

  Scenario: Test weather
    Given City name is London
    When Requesting weather information
    Then Coordinates are lon: -0.13 and lat: 51.51
    And Cloud covering are 90
    And Weather description is: Drizzle and detailed description is: light intensity drizzle
    And Weather id is: 300 and weather icon is: 09d